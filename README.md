# gfastdata

Gfastdata é uma api rEST funcionando em http1.1, http2.0 e possui a comunicação entre serviços via gRPC, mas ela não é exposta para acesso , permite somente comunicação por serviços internos.

## Endpoints

```
[GET] /v1/hero/id/:id
[GET] /v1/hero/name/:name
[GET] /ping
```

## Environment variables

```bash
export GFASTDATA_GRPC_DOMAIN='0.0.0.0'
export GFASTDATA_GRPC_PORT='50051'
export GFASTDATA_REST_EXTERNAL_DOMAIN='http://127.0.0.1'
export GFASTDATA_GRPC_NETWORK='tcp'
export GFASTDATA_GRPC_LOG_LEVEL=''

export GFASTDATA_REST_DOMAIN='0.0.0.0'
export GFASTDATA_REST_PORT='3000'
export GFASTDATA_REST_PATH_NAME='/v1/name/'
export GFASTDATA_REST_PATH_ID='/v1/id/'
```

## Deploy

```bash
$ docker build -f Dockerfile --no-cache --build-arg NETRCUSER=$USER2 --build-arg NETRCPASS=$TOKEN2 -t jeffotoni/gfastdata .
```

```bash
$ docker run --name gfastdata --rm -p 3000:3000 jeffotoni/gfastdata
```

## Curls for testing

```bash
# Get hero by ID

$ curl -i -XGET localhost:3000/v1/hero/id/13

HTTP/1.1 200 OK
Content-Type: application/json
Engine: Quick
Msguuid: 11edc59b-1f77-41e5-933f-4bcd0045d349
Date: Fri, 05 May 2023 05:19:56 GMT
Content-Length: 730
```

```bash
# Get hero by name

$ curl -i -XGET localhost:3000/v1/hero/name/a-bomb

HTTP/1.1 200 OK
Content-Type: application/json
Engine: Quick
Msguuid: 11edc59b-1f77-41e5-933f-4bcd0045d349
Date: Fri, 05 May 2023 05:19:56 GMT
Content-Length: 730
```

## Grpcurls for testing

```bash
# Get hero by ID

$ grpcurl -plaintext -d '{ "value": "13" }' \
localhost:50051 proto.HeroService/GetID
```

```bash
# Get hero by name

$ grpcurl -plaintext -d '{ "value": "a-bomb" }' \
localhost:50051 proto.HeroService/GetName

```

## Stress test results

```bash
# Get hero by ID

$ k6 run -d 90s -u 200 ./k6/get.hero.by.id.js

execution: local
    script: ./get.hero.by.id.js
    output: -

scenarios: (100.00%) 1 scenario, 200 max VUs, 2m0s max duration (incl. graceful stop):
        * default: 200 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/200 VUs, 2045791 complete and 0 interrupted iterations
default ✓ [======================================] 200 VUs  1m30s

    data_received..................: 1.8 GB  20 MB/s
    data_sent......................: 190 MB  2.1 MB/s
    http_req_blocked...............: avg=4.27µs  min=850ns   med=1.53µs  max=49.54ms  p(90)=2.07µs  p(95)=2.58µs 
    http_req_connecting............: avg=704ns   min=0s      med=0s      max=33.49ms  p(90)=0s      p(95)=0s     
    http_req_duration..............: avg=8.57ms  min=70.84µs med=6.42ms  max=95.35ms  p(90)=19.03ms p(95)=23.63ms
    { expected_response:true }...: avg=8.57ms  min=70.84µs med=6.42ms  max=95.35ms  p(90)=19.03ms p(95)=23.63ms
    http_req_failed................: 0.00%   ✓ 0            ✗ 2045791
    http_req_receiving.............: avg=71.14µs min=10.34µs med=16.95µs max=66.72ms  p(90)=33.78µs p(95)=94.07µs
    http_req_sending...............: avg=20.64µs min=4.78µs  med=6.88µs  max=77.45ms  p(90)=10.07µs p(95)=16.93µs
    http_req_tls_handshaking.......: avg=0s      min=0s      med=0s      max=0s       p(90)=0s      p(95)=0s     
    http_req_waiting...............: avg=8.48ms  min=50.77µs med=6.35ms  max=80.79ms  p(90)=18.88ms p(95)=23.43ms
    http_reqs......................: 2045791 22726.181142/s
    iteration_duration.............: avg=8.77ms  min=99.03µs med=6.57ms  max=107.16ms p(90)=19.31ms p(95)=23.99ms
    iterations.....................: 2045791 22726.181142/s
    vus............................: 200     min=200        max=200  
    vus_max........................: 200     min=200        max=200  
```

```bash
# Get hero by Name

$ k6 run -d 90s -u 200 ./k6/get.hero.by.name.js

execution: local
    script: ./k6/get.hero.by.name.js
    output: -

scenarios: (100.00%) 1 scenario, 200 max VUs, 2m0s max duration (incl. graceful stop):
        * default: 200 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/200 VUs, 1804227 complete and 0 interrupted iterations
default ✓ [======================================] 200 VUs  1m30s

    data_received..................: 1.6 GB  18 MB/s
    data_sent......................: 175 MB  1.9 MB/s
    http_req_blocked...............: avg=8.01µs  min=852ns    med=1.48µs  max=71.49ms  p(90)=1.99µs  p(95)=2.54µs 
    http_req_connecting............: avg=3.58µs  min=0s       med=0s      max=71.44ms  p(90)=0s      p(95)=0s     
    http_req_duration..............: avg=9.69ms  min=75.61µs  med=7.29ms  max=163.59ms p(90)=21.19ms p(95)=26.47ms
    { expected_response:true }...: avg=9.69ms  min=75.61µs  med=7.29ms  max=163.59ms p(90)=21.19ms p(95)=26.47ms
    http_req_failed................: 0.00%   ✓ 0            ✗ 1804227
    http_req_receiving.............: avg=86.8µs  min=10.19µs  med=16.92µs max=140.72ms p(90)=31.29µs p(95)=96.35µs
    http_req_sending...............: avg=22.95µs min=4.44µs   med=6.84µs  max=100.85ms p(90)=10.07µs p(95)=16.34µs
    http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
    http_req_waiting...............: avg=9.58ms  min=52.34µs  med=7.21ms  max=163.31ms p(90)=21.02ms p(95)=26.23ms
    http_reqs......................: 1804227 20046.249801/s
    iteration_duration.............: avg=9.93ms  min=105.62µs med=7.47ms  max=187.88ms p(90)=21.56ms p(95)=26.99ms
    iterations.....................: 1804227 20046.249801/s
    vus............................: 200     min=200        max=200  
    vus_max........................: 200     min=200        max=200  
```

#### Para teste de stress do GRPC

Poderá usar o ghz, [site](https://ghz.sh/) e poderá baixar o binário em [download ghz](https://github.com/bojand/ghz/releases)

```bash
ghz --insecure --proto grpc/proto/fastdata.proto --call proto.HeroService/GetName  \
  -c 500 \
  -z 15s \
  -d '{"value": "ajax"}' \
  localhost:50051
```