import grpc from 'k6/net/grpc';
import { check, sleep } from 'k6';

const client = new grpc.Client();
client.load(['definitions'], 'services/proto/fastdata.proto');

export default function() {
 
  client.connect('localhost:50051', {
      // plaintext: false
        plaintext: true,
  });

   const data = { value: 'ajax' };
   const response = client.invoke('proto.HeroService/GetName', data);
   check(response, {
      'status is OK': (r) => {
          //console.log('r', r)
          return r && r.status === grpc.StatusOK
      },
    });
    //console.log(JSON.stringify(response.message));
    client.close();
    //sleep(1);
};
