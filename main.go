package main

import (
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/jeffotoni/quick"
	"github.com/jeffotoni/quick/middleware/msguuid"
	"gitlab.com/jeffotoni1/eventogo/sdk/env"
	pb "gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/proto"
	grpcs "gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect/grpc/server"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
	rhero "gitlab.com/jeffotoni1/eventogo/services/gfastdata/rest/hero"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	GFASTDATA_GRPC_DOMAIN    = env.GetString("GFASTDATA_GRPC_DOMAIN", "0.0.0.0")
	GFASTDATA_GRPC_PORT      = env.GetString("GFASTDATA_GRPC_PORT", "50051")
	GFASTDATA_GRPC_NETWORK   = env.GetString("GFASTDATA_GRPC_NETWORK", "tcp")
	GFASTDATA_GRPC_LOG_LEVEL = env.GetString("GFASTDATA_GRPC_LOG_LEVEL", "")
	GFASTDATA_GRPC_ADDR      = fmts.ConcatStr(GFASTDATA_GRPC_DOMAIN, ":", GFASTDATA_GRPC_PORT)

	GFASTDATA_REST_DOMAIN          = env.GetString("GFASTDATA_REST_DOMAIN", "0.0.0.0")
	GFASTDATA_REST_PORT            = env.GetString("GFASTDATA_REST_PORT", "3000")
	GFASTDATA_REST_EXTERNAL_DOMAIN = env.GetString("GFASTDATA_REST_EXTERNAL_DOMAIN", "http://127.0.0.1")
	GFASTDATA_REST_PATH_NAME       = env.GetString("GFASTDATA_REST_PATH_NAME", "/v1/hero/name/")
	GFASTDATA_REST_PATH_ID         = env.GetString("GFASTDATA_REST_PATH_ID", "/v1/hero/id/")
	GFASTDATA_REST_ADDR            = fmts.ConcatStr(GFASTDATA_REST_DOMAIN, ":", GFASTDATA_REST_PORT)
	GFASTDATA_REST_EXTERNAL_ADDR   = fmts.ConcatStr(GFASTDATA_REST_EXTERNAL_DOMAIN, ":", GFASTDATA_REST_PORT)

	REST_URL_NAME = fmts.ConcatStr(
		GFASTDATA_REST_EXTERNAL_ADDR,
		GFASTDATA_REST_PATH_NAME,
	)

	REST_URL_ID = fmts.ConcatStr(
		GFASTDATA_REST_EXTERNAL_ADDR,
		GFASTDATA_REST_PATH_ID,
	)
)

func main() {
	go serveGrpc()
	serveRest()
}

func serveRest() {
	// connect.REST_URL_NAME = REST_URL_NAME
	// connect.REST_URL_ID = REST_URL_ID

	q := quick.New()
	q.Use(msguuid.New())
	q.Get("/v1/hero/id/:id", rhero.GetHeroByID)
	q.Get("/v1/hero/name/:name", rhero.GetHeroByName)
	q.Get("/v1/hero/list", rhero.GetHeroByNameList)
	q.Get("/ping", func(c *quick.Ctx) error {
		c.Set("Content-Type", "application/json")
		return c.Status(http.StatusOK).SendString("pong")
	})

	if err := q.Listen(GFASTDATA_REST_ADDR); err != nil {
		log.Println("error listening: ", err)
	}
}

func serveGrpc() {
	lis, err := net.Listen(GFASTDATA_GRPC_NETWORK, GFASTDATA_GRPC_ADDR)
	if err != nil {
		log.Println("failed to listen:", err)
		return
	}

	s := grpc.NewServer()
	fastcli := grpcs.New()
	pb.RegisterHeroServiceServer(s, fastcli)
	reflection.Register(s)

	fmt.Println("\033[33m[ gservices-server GRPC:", GFASTDATA_GRPC_ADDR, "]\033[0m")
	if err := s.Serve(lis); err != nil {
		log.Println("failed to serve:", err)
		return
	}
}
