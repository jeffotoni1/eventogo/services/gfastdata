package hero

import (
	"net/http"

	"github.com/jeffotoni/quick"
	e "gitlab.com/jeffotoni1/eventogo/sdk/error"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/memory"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
	"gitlab.com/jeffotoni1/eventogo/services/gfastdata/repo"
)

func GetHeroByNameList(c *quick.Ctx) error {
	hero, err := repo.GetHeroByNameList()
	if err != nil {
		switch err {
		case memory.ErrNotFound:
			err = e.NewError(fmts.ConcatStr("Heros not found."))
			return c.Status(http.StatusBadRequest).JSON(err)

		case memory.ErrInvalidAssert:
			err = e.NewError("Invalid data type stored in memomy.")
			return c.Status(http.StatusInternalServerError).JSON(err)

		default:
			err = e.NewErrorFrom(err)
			return c.Status(http.StatusInternalServerError).JSON(err)
		}
	}

	return c.Status(http.StatusOK).Send(hero) // byte
}
